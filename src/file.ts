import fileType from "file-type";
import fs from "fs";
import { promisify } from "util";
import { basename, dirname, extname, format } from "path";
import { Readable, Writable } from "stream";
import axios, { AxiosRequestConfig } from "axios";
import _ from "lodash";

const rename = promisify(fs.rename);

export const renameExtension = async (path: string): Promise<void> => {
  const type = await fileType.fromFile(path);
  if (!type) {
    return;
  }
  const { ext } = type;
  const newPath = format({
    dir: dirname(path),
    base: basename(path, extname(path)),
    ext
  });
  await rename(path, newPath);
};

export const downloadFile = async (
  url: string,
  output: Writable,
  config?: AxiosRequestConfig
): Promise<void> => {
  const defaultConfig: AxiosRequestConfig = { responseType: "stream" };
  const res = await axios.get<Readable>(url, _.merge(defaultConfig, config));
  const stream = res.data.pipe(output);
  return new Promise((resolve, reject) => {
    stream.on("finish", resolve);
    stream.on("error", reject);
  });
};
