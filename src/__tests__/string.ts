import { padZero, resolveEpisodeName, resolveSeasonName } from "../string";

describe("padZero", () => {
  test("number shorter than length", () => {
    expect(padZero(0)).toBe("00");
  });
  test("number longer than length", () => {
    expect(padZero(10)).toBe("10");
  });
  test("negative number shorter than length", () => {
    expect(padZero(-1)).toBe("-01");
  });
  test("negative number longer than length", () => {
    expect(padZero(-10)).toBe("-10");
  });
});

describe("resolveSeasonName", () => {
  test("number greater than 0", () => {
    expect(resolveSeasonName(1)).toBe("Season 01");
    expect(resolveSeasonName(10)).toBe("Season 10");
  });
  test("0", () => {
    expect(resolveSeasonName(0)).toBe("Specials");
  });
  test("negative number", () => {
    expect(() => resolveSeasonName(-1)).toThrow();
  });
  test("non-integer", () => {
    expect(() => resolveSeasonName(-1.1)).toThrow();
    expect(() => resolveSeasonName(1.1)).toThrow();
    expect(() => resolveSeasonName(NaN)).toThrow();
  });
});

describe("resolveEpisodeName", () => {
  test("without suffix", () => {
    expect(resolveEpisodeName("a", 1, 1)).toBe("a - s01e01");
    expect(resolveEpisodeName("a", 10, 10)).toBe("a - s10e10");
  });
  test("with suffix", () => {
    expect(resolveEpisodeName("a", 1, 1, "b")).toBe("a - s01e01 - b");
    expect(resolveEpisodeName("a", 10, 10, "b")).toBe("a - s10e10 - b");
  });
  test("negative number", () => {
    expect(() => resolveEpisodeName("a", -1, 1)).toThrow();
    expect(() => resolveEpisodeName("a", 1, -1)).toThrow();
  });
  test("non-integer", () => {
    expect(() => resolveEpisodeName("a", -1.1, 1)).toThrow();
    expect(() => resolveEpisodeName("a", 1.1, 1)).toThrow();
    expect(() => resolveEpisodeName("a", NaN, 1)).toThrow();
    expect(() => resolveEpisodeName("a", 1, -1.1)).toThrow();
    expect(() => resolveEpisodeName("a", 1, 1.1)).toThrow();
    expect(() => resolveEpisodeName("a", 1, NaN)).toThrow();
  });
});
