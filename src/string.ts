import _ from "lodash";

/**
 * Pad 0 to a number as a string. If number is smaller than 0, `-` is not counted.
 * @param num Number to be padded.
 * @param maxLength Max length to be padded. Default to 2.
 */
export const padZero = (num: number, maxLength = 2): string =>
  num < 0
    ? "-" + `${num * -1}`.padStart(maxLength, "0")
    : `${num}`.padStart(maxLength, "0");

export const resolveSeasonName = (season: number): string => {
  if (!_.isInteger(season) || season < 0) {
    throw new Error(`${season} is not a valid season.`);
  }
  return season === 0 ? "Specials" : `Season ${padZero(season)}`;
};

export const resolveEpisodeName = (
  title: string,
  season: number,
  episode: number,
  suffix?: string
): string => {
  if (!_.isInteger(episode) || episode < 0) {
    throw new Error(`${episode} is not a valid episode.`);
  }
  if (!_.isInteger(season) || season < 0) {
    throw new Error(`${season} is not a valid season.`);
  }
  const baseName = `${title} - s${padZero(season)}e${padZero(episode)}`;
  return suffix ? `${baseName} - ${suffix}` : baseName;
};
